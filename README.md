# Cube presentation and documentation website

- Presentation is static in `intro/`
- Documentation is generated using MkDocs from `docs` folder
- Served using Gitlab Pages @ FI MUNI gitlab