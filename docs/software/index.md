# About

Cube Software provides two main things to facilitate using the hardware and firmware:

- Cube GUI - a simple GUI application for writing
- Cube Library - a simple library to communicate with the firmware
- a script for easier loading of the measured data into Mayavi

## Installation

We currently do not provide a ready wheel that you could install through `pip`.
The recommended way is to pull the github repository and use it in your project as a local library.
The easiest way to do so is using the [example provided in the root of the github repo](https://github.com/emlab-fi/cube_software/blob/master/example.py).

There are multiple required packages to run the library:
    - `Python 3.9` or newer
    - `DearPyGui`
    - `protobuf`
    - `pySerial`

The visualization script requires these python packages:
    - `Python 3.7` or newer
    - `Mayavi`
    - `PyQt5`
    - `numpy`
    - `vtk`