# Cube GUI

![Cube GUI](https://xpecak.pages.fi.muni.cz/cube/img/gui.png)

Cube GUI is a simple, DearPyGUI, multiplatform application for controlling the cube.

Current features include:
 - Setting up serial ports for communication
 - Exposing a cmd-like interface for manual control
 - Controling movement
 - Ability to add custom sensor communication functions
 - Automated grid measurement with export to .CSV file

## GUI Sections
Each section can be collapsed and move around as a separate sub-window in top level GUI window.
The `Automated measuring` section is collapsed by default.

### Connection
Facilitates the connection via serial link to the hardware.

__Serial ports:__ Lists all current serial ports detected  
__Refresh:__ Refreshes the list of the ports  
__Connect:__ Opens connection via the selected serial port  
__Disconnect:__ Terminates the connection  


### Status
Provides various status info, such as general status, current position, current coordinate mode etc.

### Control
Has interface for movement control.

__X, Y, Z:__ Set the desired XYZ position  
__Go to position:__ Go to the set position  
__Measure:__ if the measure function is provided, runs it  
__Home:__ Start the homing procedure  

### Console
Provides a console-like control interface for the Cube.
Result of all the operations are written here.
You can type your own commands manually in the bottom text field.

#### Available commands
Type `help` to list available commands.

Detailed list of commands:

- `help`: list the available commands
- `status`: send status instruction
- `relative_pos`: get the current relative position
- `absolute_pos`: get the current absolute position
- `set_zero`: set the current position as relative zero
- `reset_zero`: reset the relative zero to absolute zero
- `home`: start the homing procedure
- `get_parameter ID`: get the parametr `ID`'s value. `ID` is decimal `uint32_t`.
- `set_parameter ID VALUE`: set the paramter `ID` to `VALUE`. Both `ID` and `VALUE` are decimal `uint32_t`.
- `set_coordinate_mode {cartesian|cylindrical|spherical}`: sets the coordinate mode
- `move X Y Z`: move to a position. `X Y Z` are decimal floats.
- `get_gpio INDEX{0-15}`: get value of GPIO of selected index
- `set_gpio INDEX{0-15} BOOL`: set value of GPIO of selected index
- `set_gpio_mode INDEX{0-15} BOOL`: set the GPIO to input or output
- `i2c_transfer RX_LEN{0-64} TX_LEN{0-64} ADDR DATA`: make a I2C transfer to device with address `ADDR`. Sends `RX_LEN` bytes of `DATA`, then receives `TX_LEN` bytes. `DATA` has to be a string of hexadecimal values with no spaces (ie, `0x74 0xAF 0x01` should be `74AF01`). `ADDR` is also a hex value with no prefix (ie, `58` or `7C`).
- `spi_transfer CS{0-3} MODE{0-3} LEN{0-64} DATA`: make an SPI transfer of length `LEN` on the bus. Use chip select with index `CS`. The bus will use `MODE` for the CPOL/CPHA setting number (refer to wikipedia). `DATA` has to be a string of hexadecimal values with no spaces (ie, `0x74 0xAF 0x01` should be `74AF01`).


### Automated measuring
Can be used to do an automated grid measurement and save results to an output file.
The scanned volume  is a grid in the current coordinate system, so for cylindrical and spherical modes, it is not a cube, but a cylinder and sphere, respectively.

It is required to provide the measure and init functions to use this functionality.
The software will step through all the grid positions, run the measurement function and save the output data to a .csv file.

__Start position:__ The start position of the grid  
__Step size:__ Spacing between measurement points  
__Number of steps:__ How many steps per axis  
__Set current position:__ Sets the current position as the start position  
__Select save location:__ Opens dialog to change the output file location and name  
__Start measuring:__ Starts the measurement process  
__Stop measuring:__ Should stop the measurement process, but currently does not work  
__Progress:__ Shows current progress


## Supplying custom sensor functions to the GUI
You can provide your own `measure` and `init` functions when launching the GUI.
These functions enable you to implement custom interaction with the sensor.
Their interface is simple:

- input argument is a `CubeComm` object from the `PyCubeLib`
- you use this object to interact with the cube - talk to a sensor, do some submovement
- return argument of `measure` is a tuple `(has_error, error, (x, y ,z))`, where:
    - `has_error` is a bool indicating error
    - `error` is the error string
    - `(x, y, z)` is the output tuple with XYZ measurement data (you can use only a single element, but the API always requires three)
- return argument of `init` is a boolean value, indicating either successful or failed initialization

Look into the provided `example.py` for a basic usage example.


## CSV export format
A simple .csv file with each line representing a six-dimensional vector:
first three elements are the XYZ position in the coordinate system they were measured in,
the rest are XYZ data of the measurement.

```csv
x_pos, y_pos, z_pos, x_val, y_val, z_val  # header
px1, py1, pz1, x1, y1, z1                 # measurement data
px2, py2, pz2, x2, y2, z2
px3, py3, pz3, x3, y3, z3
...
...
...
```