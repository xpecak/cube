# `vis_launch.py` usage
Launch: `$  python vis_launch.py DATA [FLAGS]`

Requires:
    - `Mayavi`
    - `PyQt5`
    - `numpy`
    - `vtk`

This app loads the data from file with same format as the output file from `Cube GUI` and loads them into a Mayavi visualization. It prepares the vector data source, a vector normal data source and the three scalar sources of the vector components.

`DATA` is the path to the datafile to load.

## Flags

`-v, --vectors` - force the rendering of quiver plot. 
By default, if any flags are specified, the quiver plot is not rendered. 
This forces the rendering.

`-f, --fieldlines` - render the field lines (streamlines). 
It defaults to a x-axis oriented plane source, with grid size of 15. 
The position of the seed still needs to be adjusted manually for best results.

`-i NUM, --isosurf NUM` - renders isosurfaces(equipotentional surfaces), with NUM evenly spaced levels. 
Defaults to 5 levels if not NUM is specified.

`-s SLICE, --slice SLICE` - Add a slice. Multiple slices can be added by repeating the argument. The format is two letters, where the first letter is the axis of slice.
Second letter is the vector component to visualize.
`s` is the scalar size of the vector. 
If the second character is omited, the slice is a vector field slice. 
EXAMPLE: `-s xy -s x -s zx`
