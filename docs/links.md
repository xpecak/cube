# Links

- [Cube project homepage](https://xpecak.pages.fi.muni.cz/cube)
- [cube_electronics repository](https://github.com/emlab-fi/cube_electronics)
- [cube_firmware repository](https://github.com/emlab-fi/cube_firmware)
- [cube_software repository](https://github.com/emlab-fi/cube_software)
- [cube_mechanical repository](https://github.com/emlab-fi/cube_mechanical)