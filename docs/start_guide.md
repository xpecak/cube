# Cube project start guide

This documentation provides general overview of how the Cube project works and how to use it.
It gives both an overview for the user, and for potential developers, which seek to modify any component of Cube for their purpose.

## About

Cube is a project that aims to provide an universal platform for measurement of any sensor data in 3 dimensions, or other similar uses.
More specifically, it can position sensor (or any other device, such as syringe) in 3 dimensions, and then read data from the sensor (or control the attached device).
It can store the received data and also provides a simple script to launch the visualization of the data.

Historically, Cube was designed to measure magnetic fields using a common sensor, and visualizing such data.
You can read more information about the old version [in the docs](old_version_docs.md)

## Components

Cube is designed as a set of standalone components that work together.
This is done so that it is easy to switch one of the components, or use just one or two of the components, rather than having to use the whole project.
For example, you can use the electronics and software, while using a 3d printer mechanical part of your own.
Or you can just use the mechanical part, electronics and firmware, and write your own control software.
There are 4 main components as of now.

- __Cube Mechanical__ - a simple, 3d printer inspired mechanical CNC, with CoreXY mechanics
- __Cube Electronics__ - electronics for controlling stepper-based, 3-axis CNC and for providing communication interface for the sensor and the controlling PC
- __Cube Firmware__ - firmware residing on the Cube Electronics, or for porting to your own electronics, which facilitates communication with and control of the electronics
- __Cube Software__ - Python library providing a GUI control application, low level API for communication and control of the firmware and simple visualization

You can read detailed guide to each component in the docs, in the component's section.
All components are licensed under copy-left open source licenses, more detail about the licensing in the Docs of the components or in the [Licensing docs](license.md)

## General workflow

If you with to use a sensor with Cube, the workflow is same for majority of the cases.

0. Read the docs here and familiarize yourself with the Cube project.
1. Mount the sensor to the mechanical part. If you use Cube Mechanical, we provide a guide [here](mechanical/index.md). 
2. Connect the sensor to the interface of the electronics. If you decide to use Cube Electronics, we again provide a [guide](electronics/index.md).
3. Write a software to communicate with the sensor via the firmware and electronics. You can read about using the API and the app in the [basic usage guide](software/index.md).
4. Measure (or whatever you do with the sensor)

Each of the steps is related to one of the components of the Cube.
If you use the provided Cube components, you can read more documentation for each step in the documentation provided here.

## Building and using Cube

The Cube project is still in somewhat early and unstable development, so there is no stable version.
If you wish to build, use or modify Cube, it's currently best to contact us for further assistance.
Do not forget to read about [the licensing](license.md) of the Cube project if you want to modify it or base your device/product on it.
You can download all source code and manufacturing data from the repositories [linked here](links.md)

## Contact

Best way to contact us is to contact the project mantainer, Oldrich Pecak, by email, at `oldrich.pecak@mail.muni.cz`