# Licensing

All source code, PCB design files and mechanical design files for the Cube project is released under copy-left license.
As the project is composed of several components, some being software, some being hardware, we do not use a single license to cover the whole project.
We primarily use two licenses:

- __CERN-OHL-W-2.0__ for the hardware - electronics and mechanical parts of Cube
- __MPL 2.0__ for the software and firmware

If you want to base your own project or product on the Cube project, you need to abide by these two licenses, more details for each of them below.

## CERN-OHL-W-2.0
Also known as CERN Open Hardware License.
This license is weakly reciprocal, which puts you under the following requirements:

- Keep intact all the copyright and trademark notices, references to the license and disclaimers in the original source.
- When modifying them, you have to provide list of changes compared to original, and license the changes under the same license.

To read more details about the license, read the license text [here](https://ohwr.org/cern_ohl_w_v2.pdf) and the user guide [here](https://ohwr.org/project/cernohl/wikis/uploads/c2e5e9d297949b5c2d324a6cbf6adda0/cern_ohl_w_v2_howto.pdf).

## MPL 2.0
Also known as Mozilla Public License.
It is also a copy-left license, designed such that any modifications must be licensed and published the same way, but you can combine our software and firmware with other licenses.
There are similar requirements as to the CERN OHL:

- Keep intact all the original copyright and trademark notices, references to license and disclaimers.
- Any program or library you are distributing has to disclose which part is under MPL, and you have to provide the changes you have done, also under MPL.

To read more details about the license, have a look at the [license text](https://www.mozilla.org/en-US/MPL/2.0/) and the [FAQ](https://www.mozilla.org/en-US/MPL/2.0/FAQ/), which most likely answers most of your questions.