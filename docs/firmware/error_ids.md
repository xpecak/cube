# Error Codes

Cube firmware uses it's own way to encode errors into numbers, which is described bellow.
You can also find the table of all current error codes here.

## General description
- 32bit unsigned int
- the 32bits are divided such that top 8bits are `namespace`, lower 24bits are `error_code`
- `error_code` is further divided,  higher 8bits `error_category` and lower 16bits `error_id`
- the complete error code is calculated as follow: `namespace + error_category + error_id`
- all zeroes correspond to no error

## Error code table

|namespace|category|error_id|description|
|:--------:|:------:|:------:|:---------:|
|0xAA|0x01|0x0001|message is not a command message|
|    |0x02|0x0001|message decoding error|
|    |    |0x0002|message wrong payload|
|    |0x03|0x0001|planner out of bounds|
|    |    |0x0002|planner conversion error|
|    |    |0x0003|planner misc error|
|    |0x05|0x0001|error while in velocity mode|
|    |    |0x0002|error while in step mode|
|    |    |0x0003|unexpected endstop triggered|
|    |    |0x0004|reset position error|
|    |0x06|0x0001|gpio set mode error|
|    |    |0x0002|gpio wrong mode|
|    |    |0x0003|gpio set error|
|    |    |0x0004|gpio read error|
|    |    |0x0005|gpio misc error|
|    |    |0x000a|i2c error|
|    |    |0x000b|spi error|
|    |0x08|0x0001|fatal error in cube_lib::controller|
|    |    |0x0002|instruction not implemented|


|category id|category name|
|----------:|:-----------:|
|0x01|message|
|0x02|decode|
|0x03|planner|
|0x04|hw_transfers|
|0x05|hw_movement|
|0x06|hw_io|
|0x07|hw_misc|
|0x08|misc|
