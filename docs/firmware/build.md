# Build and debug

## Required environment

We currently support building the cube_firmware only on Linux, more notably Ubuntu 20.04 and higher.
Windows subsystem for linux and other system should work, but we do not provide support or help with getting the firmware to compile on them.
There are multiple dependencies you need to have in your environment:

- CMake, ver >= 3.16
- arm-none-eabi-gcc toolchain, with GCC version >= 10
- normal desktop GCC, ver >= 10 (for tests)
- ProtocolBuffers compiler
- Nanopb python package

Each target (`tests`, `firmare` and `firmware_backport`) has it's own CMake project. The build process is similar for each of them:

```bash
mkdir build
cd build
cmake ..
make
```

For both firmware targets, you need to specify the arm-none-eabi-gcc toolchain path, look into the `CMakeList.txt` and `armgcc.cmake` files to where to set them.

## Debugging

To aid in uploading and debugging the code on the Cube Electronics, we provide a config for `cortex-debug` plugin for VSCode in both `firmware` and `firmware_backport`. Both of them have a different setup.

- `firmware` - requires STLink debug probe and OpenOCD package installed
- `firmware_backport` - requires Segger JLink packages and the K64F devkit

## Regenerating protocols

If you modify the communication protocol defined in `cube_lib/protocols/cube.proto`, you need to regenerate the C header and source files. We provide a simple bash script to do this, which is located in `cube_lib/generate_proto.sh`.