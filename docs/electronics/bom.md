# BOM (Bill of Materials)

Interactive BOM for the main board [can be found here](https://xpecak.pages.fi.muni.cz/cube/main_board_rev_1_bom.html).

Interactive BOM for the interface board [can be found here](https://xpecak.pages.fi.muni.cz/cube/interface_board_rev_2.1_bom.html).